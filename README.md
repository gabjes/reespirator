# Reespirator

This code is meant to provide an open-source baseline for a medical test.

## Resources

* [Arduino Reference](https://www.arduino.cc/reference/en/)
* [Arduino Best Practices and Gotchas](https://www.theatreofnoise.com/2017/05/arduino-ide-best-practices-and-gotchas.html)
* [C++ Best Practices (Gitbook)](https://lefticus.gitbooks.io/cpp-best-practices/content/)
